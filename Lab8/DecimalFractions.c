/* file: DecimalFractions.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 24/10/2018 */
/* version: 0.0.1 */
/* Description: For a given integer n, outputs the 0.X[Y] decimal representation
    for 1 / n, where X is the non-repeating portion, and Y is the repeating portion */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void *safeMalloc(size_t size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeRealloc(void *p, size_t size) {
    void *temp = realloc(p, size);
    if (temp == NULL) {
        printf("No more memory? realloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return temp;
}

typedef struct IntSet {
    int *numbers;
    int current_max;
    int current_size;
} IntSet_t;

// functions for IntSet
void initIntSet(IntSet_t *is, int initial_size) {
    is->numbers = safeMalloc(initial_size * sizeof(int));
    for (int i = 0; i < initial_size; i++) {
        is->numbers[i] = -1;
    }
    is->current_size = 0;
    is->current_max = initial_size;
}

void addInt(IntSet_t *is, int x) {
    // resize if necessary
    if (is->current_size == is->current_max) {
        is->current_max *= 2;
        is->numbers = safeRealloc(is->numbers, is->current_max * sizeof(int));
        for (int i = is->current_size; i < is->current_max; i++) {
            is->numbers[i] = -1;
        }
    }
    is->numbers[is->current_size++] = x;
}
void printIntSet(IntSet_t is) {
    for (int i = 0; i < is.current_max; i++) {
        printf("%d ", is.numbers[i]);
    }
    printf("\n");
}

int indexOf(int val, IntSet_t *is) {
    int first_idx = -1;
    for (int i = 0; i < is->current_size; i++) {
        if (is->numbers[i] == val) {
            first_idx = i;
            break;
        }
    }
    return first_idx;
}

void freeIntSet(IntSet_t *is) {
    free(is->numbers);
    is->numbers = NULL;
    is->current_max = 0;
    is->current_size = 0;
}
// end functions for IntSet

void printHead(IntSet_t *digits, int tail_start_index) {
    for (int i = 0; i < tail_start_index; i++) {
        printf("%d", digits->numbers[i]);
    }
}

void printTail(IntSet_t *digits, int tail_start_index, int tail_length) {
    printf("[");
    for (int i = tail_start_index, count = 0; count < tail_length; count++, i++) {
        printf("%d", digits->numbers[i]);
    }
    printf("]");
}

void findHeadTailForOneOverNumber(int num, IntSet_t *digits, IntSet_t *remainders) {
    int
        prev = 1,
        index_of_repeated_remainder = -1,
        current_index = 0;

    while (index_of_repeated_remainder == -1) {
        int
            prev10 = prev * 10,
            res = prev10 / num,
            rem = res * num;
            
        index_of_repeated_remainder = indexOf(prev10, remainders);
        addInt(digits, res);
        addInt(remainders, prev10);
        prev = prev10 - rem;
        current_index++;
    }
    
    printf("0.");
    printHead(digits, index_of_repeated_remainder);
    printTail(digits, index_of_repeated_remainder, current_index - index_of_repeated_remainder - 1);
}

int main(int argc, char *argv[]) {
    int n;
    scanf("%d", &n);

    IntSet_t
        digits,
        remainders;
    initIntSet(&digits, 8);
    initIntSet(&remainders, 8);

    findHeadTailForOneOverNumber(n, &digits, &remainders);

    freeIntSet(&digits);
    freeIntSet(&remainders);

    return 0;
}