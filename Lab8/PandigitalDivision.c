/* file: PandigitalDivision.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 23/10/2018 */
/* version: 0.0.1 */
/* Description: For a given integer n, finds all pandigital divisions that
    result in n */

#include <stdio.h>
#include <stdlib.h>

void *safeMalloc(size_t size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeCalloc(size_t size) {
    void *p = calloc(size, size);
    if (p == NULL) {
        printf("No more memory? calloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeRealloc(void *p, size_t size) {
    void *temp = realloc(p, size);
    if (temp == NULL) {
        printf("No more memory? realloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return temp;
}

static int pow10(int n) {
    static int max = 9;
    if (n > max) {
        printf("Can't do pow10(%d). max is %d\n", n, max);
        exit(EXIT_FAILURE);
    }
    static int pow10[10] = {
        1, 10, 100, 1000, 10000, 
        100000, 1000000, 10000000, 100000000, 1000000000
    };
    return pow10[n]; 
}

typedef struct NumDiv {
    int numerator;
    int denominator;
} NumDiv_t;

typedef struct NumDivSet {
    NumDiv_t *numbers;
    int current_max;
    int current_size;
} NumDivSet_t;

// functions for NumDivSet
void initNumDivSet(NumDivSet_t *nds, int initial_size) {
    nds->numbers = safeMalloc(initial_size * sizeof(NumDiv_t));
    nds->current_size = 0;
    nds->current_max = initial_size;
}

void addItem(NumDivSet_t *nds, NumDiv_t to_add) {
    // resize if necessary
    if (nds->current_size == nds->current_max) {
        nds->current_max *= 2;

        nds->numbers = safeRealloc(nds->numbers, nds->current_max * sizeof(NumDiv_t));
    }
    nds->numbers[nds->current_size++] = to_add;
}

void printNumDivSet(NumDivSet_t nds) {
    for (int i = 0; i < nds.current_size; i++) {
        NumDiv_t n = nds.numbers[i];
        if (n.numerator < 1 * pow10(4)) {
            printf("0%d/%d\n", n.numerator, n.denominator);
        } else if (n.denominator < 1 * pow10(4)) {
            printf("%d/0%d\n", n.numerator, n.denominator);
        } else {
            printf("%d/%d\n", n.numerator, n.denominator);
        }
    }
}

void swapNumDiv(NumDiv_t *a, NumDiv_t *b) {
    NumDiv_t temp = *a;
    *a = *b;
    *b = temp;
}

int getQsPivot(NumDivSet_t *nds, int low_idx, int high_idx) {
    int pivot_num = nds->numbers[high_idx].numerator;
    int smallest_el_idx = low_idx - 1;
    for (int i = low_idx; i <= high_idx - 1; i++) {
        if (nds->numbers[i].numerator >= pivot_num) {
            smallest_el_idx++;
            swapNumDiv(nds->numbers + smallest_el_idx, nds->numbers + i);
        }
    }
    swapNumDiv(nds->numbers + smallest_el_idx + 1, nds->numbers + high_idx);
    return smallest_el_idx + 1;
}
void quickSort(NumDivSet_t *nds, int low_idx, int high_idx) {
    if (low_idx < high_idx) {
        int pivot_idx = getQsPivot(nds, low_idx, high_idx);
        quickSort(nds, low_idx, pivot_idx - 1);
        quickSort(nds, pivot_idx + 1, high_idx);
    }
}

void sortNumDivSet(NumDivSet_t *nds) {
    quickSort(nds, 0, nds->current_size - 1);
}

void freeNumDivSet(NumDivSet_t *nds) {
    free(nds->numbers);
    nds->numbers = NULL;
    nds->current_max = 0;
    nds->current_size = 0;
}
// end functions for FreqSet

void swap(int *a, int *b) {
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

void printArr(int *arr, int len) {
    for (int i = 0; i < len; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void findWorkingPandigDivs(NumDivSet_t *working_num_divs, int num_to_get, int *numbers, int length, int original_length) {
    if (length == 1) {
        // this is a new permutation of the numbers
        int
            numerator = 0,
            denominator = 0;
        // the first 5 digits of our 10 digit number are the numerator
        for (int i = 0; i < original_length / 2; i++) {
            numerator += numbers[i] * pow10(i);
        }
        // the next 5 are the denominator
        for (int i = original_length / 2, mul10 = 0; i < original_length; i++, mul10++) {
            denominator += numbers[i] * pow10(mul10);
        }
        if ((double)numerator / (double)denominator == (double)num_to_get) {
            NumDiv_t this_one_works;
            this_one_works.numerator = numerator;
            this_one_works.denominator = denominator;
            addItem(working_num_divs, this_one_works);
        }
        return;
    } else {
        for (int i = 0; i < length; i++) {
            findWorkingPandigDivs(working_num_divs, num_to_get, numbers, length - 1, original_length);
            if (length % 2 == 0) {
                swap(&numbers[i], &numbers[length-1]);
            } else {
                swap(&numbers[0], &numbers[length-1]); 
            }
        }
    }
}

int main(int argc, char *argv[]) {
    int
        numbers[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
        number_to_get;
    scanf("%d", &number_to_get);

    NumDivSet_t nds;
    initNumDivSet(&nds, 8);
    
    findWorkingPandigDivs(&nds, number_to_get, numbers, 10, 10);

    sortNumDivSet(&nds);
    
    if (nds.current_size > 0) {
        printNumDivSet(nds);
    } else {
        printf("NONE");
    }

    return 0;
}