/* file: ArithmeticExpressions.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 28/10/2018 */
/* version: 0.0.1 */
/* Description: Calculates how many combinations of operations on
    integers 1 - 9 produce a given integer */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void printArr(int *arr, int len) {
    for (int i = 0; i < len; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

bool mostlyThrees(int *arr, int len) {
    int three_count = 0;
    for (int i = 0; i < len; i++) {
        if (arr[i] == 3) {
            three_count++;
        }
    }
    return three_count > 6;
}

int resultOfOp(int operation, int left, int right) {
    switch (operation) {
    case 1: return left + right;
    case 2: return left - right;
    case 3: return left * right;
    default:
        printf("Unknown op %d\n", operation);
        exit(EXIT_FAILURE);
    }
}

char getOpChar(int operation) {
    return operation == 1
        ? '+'
        : operation == 2
            ? '-'
            : 'x';
}

void shiftItems(int *arr, int idx, int length) {
    for (int i = idx; i < length - 1; i++) {
        arr[i] = arr[i + 1];
    }
    arr[length - 1] = 0;
}

void groupMultiples(int *operations, int *numbers, int *length) {
    int original_length = *length, number_index = 0;

    for (int i = 0; i < *length - 1; i++) {
        if (operations[i] == 3) {

            numbers[number_index + 1] *= numbers[number_index];
            shiftItems(numbers, number_index, original_length);

            shiftItems(operations, i, original_length - 1);
            i--;
            *length -= 1;
        } else {
            number_index++;
        }
    }
}


bool combinationWorks(int *operations, int num_ops, int number_to_get) {
    int 
        length = num_ops + 1,
        numbers[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9},
        local_operations[8];

    for (int i = 0; i < 8; i++) {
        local_operations[i] = operations[i];
    }

    groupMultiples(local_operations, numbers, &length);

    int total = numbers[0];
    for (int i = 1; i < length; i++) {
        total = resultOfOp(local_operations[i - 1], total, numbers[i]);
    }

    return total == number_to_get;
}

void findWorkingOperations(int *operations, int position, int num_ops, int number_to_get, int *count) {
    for (int o = 1; o <= 3; o++) {
        operations[position] = o;
        if (position == 7) {
            // now we have a unique ops combination
            if (combinationWorks(operations, num_ops, number_to_get)) {
                *count += 1;
            }
        } else {
            findWorkingOperations(operations, position + 1, num_ops, number_to_get, count);
        }
    }
}

int main(int argc, char *argv[]) {
    int
        number_to_get,
        count = 0,
        operations[8] = {0};

    scanf("%d", &number_to_get);

    findWorkingOperations(operations, 0, 8, number_to_get, &count);

    printf("%d", count);
    return 0;
}