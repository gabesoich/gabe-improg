/* file: Transitivity.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 25/10/2018 */
/* version: 0.0.1 */
/* Description: Given a set of A < B relationships where
    A and B are lowercase letters, outputs an alphabetically
    sorted list of relationships */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void *safeCalloc(size_t size) {
    void *p = calloc(size, size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void lessThan(int letter_index, bool relationships[26][26], bool *less_than) {
    for (int i = 0; i < 26; i++) {
        if (relationships[letter_index][i] == true) {
            less_than[i] = true;
            lessThan(i, relationships, less_than);
        }
    }
}

int main(int argc, char *argv[]) {
    bool relationships[26][26];

    for (int i = 0; i < 26; i++) {
        for (int j = 0; j < 26; j++) {
            relationships[i][j] = false;
        }
    }

    while (true) {
        char a, b;
        int success = scanf("%c<%c\n", &a, &b);
        if (success == 2) {
            relationships[a - '`' - 1][b - '`' - 1] = true;
        } else {
            break;
        }
    }

    // for (int i = 0; i < 26; i++) {
    //     for (int j = 0; j < 26; j++) {
    //         printf("%d ", relationships[i][j]);
    //     }
    //     printf("\n");
    // }

    for (int i = 0; i < 26; i++) {
        bool *less_than = safeCalloc(26 * sizeof(char));
        lessThan(i, relationships, less_than);
        bool
            printed_letter = false,
            first_letter = true;

        for (int j = 0; j < 26; j++) {
            if (less_than[j] == true) {
                if (!printed_letter) {
                    printed_letter = true;
                    printf("%c<", i + 1 + '`');
                }
                if (!first_letter) {
                    printf(",");
                }
                first_letter = false;
                printf("%c", j + 1 + '`');
            }
        }
        if (printed_letter) {
            printf("\n");
        }
        free(less_than);
    }

    return 0;
}