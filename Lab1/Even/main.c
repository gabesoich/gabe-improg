/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 03/09/2018 */
/* version: 0.0.1 */
/* Description: Figures out whether or not a number is even */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool numberIsOdd(int num) {
    return num % 2 == 1;
}

int main(int argc, char *argv[]) {

    int number;
    
    scanf("%i", &number);

    int first_digit = number / 1000;
    int second_digit = number / 100 - (10 * first_digit);
    int third_digit = number / 10 - (100 * first_digit + 10 * second_digit);
    int fourth_digit = number - (1000 * first_digit + 100 * second_digit + 10 * third_digit);

    if (numberIsOdd(first_digit) || numberIsOdd(second_digit) || numberIsOdd(third_digit) || numberIsOdd(fourth_digit)) {
        printf("%i is not an even digit number.", number);
    } else {
        printf("%i is an even digit number.", number);
    }

    return 0;
}
