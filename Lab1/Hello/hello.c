/* file: hello.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 03/09/2018 */
/* version: 0.0.1 */
/* Description: This program prints ’Hello world’ */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    printf("Hello world!\n");
    return 0;
}
