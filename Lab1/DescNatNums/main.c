/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 04/09/2018 */
/* version: 0.0.1 */
/* Description: Receives a 4 digit number,
    outputs a descending natural number with the same digits */

#include <stdio.h>
#include <stdlib.h>

void swap(int *array, int idx1, int idx2) {
    int temp = array[idx1];
    array[idx1] = array[idx2];
    array[idx2] = temp;
}

int main(int argc, char *argv[]) {

    int number;
    int digits[4];
    
    scanf("%i", &number);

    // get the individual digits
    digits[0] = number / 1000;
    digits[1] = number / 100 - (10 * digits[0]);
    digits[2] = number / 10 - (100 * digits[0] + 10 * digits[1]);
    digits[3] = number - (1000 * digits[0] + 100 * digits[1] + 10 * digits[2]);

    // Would be nice to use loops here.
    // Go through each number from the last position, and swap
    // numbers where the later number is larger than the earlier one.
    // Repeat 3 times to sort the list
    if (digits[3] > digits[2]) {
        swap(digits, 3, 2);
    }
    if (digits[2] > digits[1]) {
        swap(digits, 2, 1);
    }
    if (digits[1] > digits[0]) {
        swap(digits, 1, 0);
    }
    if (digits[3] > digits[2]) {
        swap(digits, 3, 2);
    }
    if (digits[2] > digits[1]) {
        swap(digits, 2, 1);
    }
    if (digits[1] > digits[0]) {
        swap(digits, 1, 0);
    }
    if (digits[3] > digits[2]) {
        swap(digits, 3, 2);
    }
    if (digits[2] > digits[1]) {
        swap(digits, 2, 1);
    }
    if (digits[1] > digits[0]) {
        swap(digits, 1, 0);
    }

    printf("%i%i%i%i", digits[0], digits[1], digits[2], digits[3]);

    return 0;
}
