/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 03/09/2018 */
/* version: 0.0.1 */
/* Description: Calculates the amount of bottles needed for a party */

#include <stdio.h>
#include <stdlib.h>

int customCeil(double number) {
    int int_of_num = (int)number;
    return int_of_num == (float)number ? int_of_num : int_of_num + 1;
}

int main(int argc, char *argv[]) {

    int
        glass_size = 150,
        bottle_size = 1500,
        num_friends;
    
    scanf("%i", &num_friends);

    /* 
        Num friends + 1 to include John.
        We figure out the exact number of bottles we need, and round up.
     */
    int bottles_required = customCeil((double)((num_friends + 1) * glass_size) / (double)bottle_size);

    printf("%i", bottles_required);

    return 0;
}
