/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 04/09/2018 */
/* version: 0.0.1 */
/* Description: Calculates the overlap of 2 rectangles
    defined by 2 corner points on an integer grid */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct Point {
    int x,y;
} Point;

typedef struct Rectangle {
    struct Point *corner_1, *corner_2;
} Rectangle;

void swapY(Point *a, Point *b) {
    a->y = b->y + a->y;
    b->y = a->y - b->y;
    a->y = a->y - b->y;
}

int min(int a, int b) {
    return a > b ? b : a;
}
int max(int a, int b) {
    return a > b ? a : b;
}

int getOverlapArea(Rectangle r1, Rectangle r2) {
    // store the correct point as leftmost or rightmost
    Point *leftmost_1 = r1.corner_1->x < r1.corner_2->x ? r1.corner_1 : r1.corner_2;
    Point *leftmost_2 = r2.corner_1->x < r2.corner_2->x ? r2.corner_1 : r2.corner_2;

    Point *rightmost_1 = r1.corner_1->x > r1.corner_2->x ? r1.corner_1 : r1.corner_2;
    Point *rightmost_2 = r2.corner_1->x > r2.corner_2->x ? r2.corner_1 : r2.corner_2;

    // if leftmost is not lowest, swap y co-ord to make it lowest
    if (leftmost_1->y > rightmost_1->y) {
        swapY(leftmost_1, rightmost_1);
    }
    if (leftmost_2->y > rightmost_2->y) {
        swapY(leftmost_2, rightmost_2);
    }

    // at this point, rightmost is top, leftmost is bottom
    int overlap_x = min(rightmost_1->x, rightmost_2->x) - max(leftmost_1->x, leftmost_2->x);
    if (overlap_x <= 0) return 0;
    
    int overlap_y = min(rightmost_1->y, rightmost_2->y) - max(leftmost_1->y, leftmost_2->y);
    if (overlap_y <= 0) return 0;

    return overlap_x * overlap_y;
}


int main(int argc, char *argv[]) {

    Rectangle rectangle_1, rectangle_2;
    Point point_1, point_2, point_3, point_4;
    
    scanf("%i %i %i %i", &point_1.x, &point_1.y, &point_2.x, &point_2.y);
    scanf("%i %i %i %i", &point_3.x, &point_3.y, &point_4.x, &point_4.y);

    rectangle_1.corner_1 = &point_1;
    rectangle_1.corner_2 = &point_2;
    rectangle_2.corner_1 = &point_3;
    rectangle_2.corner_2 = &point_4;

    int overlap_area = getOverlapArea(rectangle_1, rectangle_2);

    printf("%i", overlap_area);

    return 0;
}
