/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 18/09/2018 */
/* version: 0.0.1 */
/* Description: Sums an integers proper divisors, and finds whether or not 
that sum is the reverse of the number */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

bool isPrime(int num) {
    if (num <= 1) {
        return false;
    } else if (num <= 3) {
        return true;
    } else if (num % 2 == 0 || num % 3 == 0) {
        return false;
    }
    // uses (6k + 1)<=sqrt(num) to test primality
    // for numbers > 3
    int i = 5;
    while (i * i <= num) {
        if (num % i == 0 || num % (i + 2) == 0) {
            return false;
        }
        i += 6;
    }
    return true;
}

int numLength(int num) {
    return (num > 999999999)
        ? 10 : (num > 99999999)
        ? 9 : (num > 9999999)
        ? 8 : (num > 999999)
        ? 7 : (num > 99999)
        ? 6 : (num > 9999)
        ? 5 : (num > 999)
        ? 4 : (num > 99)
        ? 3 : (num > 9)
        ? 2 : (num > 0)
        ? 1 : 0;
}

int getDigit(int num, int digit) {
    return (num / (int)pow((double)10, (double)(digit - 1))) % 10;
}


bool isSuperPrime(int num) {
    int
        length = numLength(num),
        sub_number,
        power_of_10;

    // leave early if not prime or shorter than 2 digits
    if (!isPrime(num) || length < 2) {
        return false;
    }
    // we have to remove each digit once
    for (int i = length; i > 0; i--) {
        sub_number = 0;
        power_of_10 = 0;
        // need to go through all of the digits
        for (int j = 1; j <= length; j++) {
            if (i != j) {
                int multiple = (int)pow((double)10, (double)power_of_10);
                int to_add = getDigit(num, j) * multiple;
                sub_number += to_add;
                power_of_10++;
            }
        }
        if (sub_number == 1 || !isPrime(sub_number)) {
            return false;
        }
    }
    return true;

}

int main(int argc, char *argv[]) {

    int
        given_integer,
        number_of_superprimes_found = 0,
        current_superprime,
        number_to_check = 3;
    scanf("%d", &given_integer);

    while (number_of_superprimes_found < given_integer) {
        if (isSuperPrime(number_to_check)) {
            current_superprime = number_to_check;
            number_of_superprimes_found++;
        }
        number_to_check++;
    }

    printf("%d", current_superprime);

    return 0;
}
