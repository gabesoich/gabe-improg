/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 17/09/2018 */
/* version: 0.0.1 */
/* Description: Applies a given sequence of functions to a given integer */

#include <stdio.h>
#include <stdlib.h>

int f(int x) {
    return x + 1;
}

int g(int x) {
    return 3 * x + 1;
}

int h(int x) {
    return x * x - x + 42;
}

int main(int argc, char *argv[]) {

    int
        given_integer,
        result;
    char current_function;
    scanf("%d", &given_integer);
    result = given_integer;
    do {
        scanf("%c", &current_function);
        switch(current_function) {
        case 'f':
            result = f(result);
            break;
        case 'g':
            result = g(result);
            break;
        case 'h':
            result = h(result);
            break;
        }
    } while(current_function != '=');

    printf("%d", result);

    return 0;
}
