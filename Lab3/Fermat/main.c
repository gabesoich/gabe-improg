/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 18/09/2018 */
/* version: 0.0.1 */
/* Description: determines whether a given integer is a prime,
    otherwise returns the lowest integer witness for it, or, if none
    is found, that it is a Carmichael number. */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

bool isPrime(int num) {
    if (num <= 1) {
        return false;
    } else if (num <= 3) {
        return true;
    } else if (num % 2 == 0 || num % 3 == 0) {
        return false;
    }
    // uses (6k + 1)<=sqrt(num) to test primality
    // for numbers > 3
    int i = 5;
    while (i * i <= num) {
        if (num % i == 0 || num % (i + 2) == 0) {
            return false;
        }
        i += 6;
    }
    return true;
}

int modularPower(int base, int exp, int mod) {
    // currently implemented usin
    // right to left binary methods
    int result = 1;
    while (exp > 0) {
        if (exp % 2 == 1) {
            result = result * base % mod;
        }
        exp = exp >> 1;
        base = base * base % mod;
    }
    return result;
}

int lowestWitness(int num) {
    int lowest_witness = 0;
    for (int a = 2; a < num; a++) {
        if (modularPower(a, num, num) != a) {
            lowest_witness = a;
            break;
        }
    }
    return lowest_witness;
}

int main(int argc, char *argv[]) {

    int given_integer;
    scanf("%d", &given_integer);

    if (isPrime(given_integer)) {
        printf("%d is prime.", given_integer);
    } else {
        int smallest_witness = lowestWitness(given_integer);
        if (smallest_witness != 0) {
            printf("%d is a witness for %d.", smallest_witness, given_integer);
        } else {
            printf("%d is a Carmichael number.",given_integer);
        }
    }

    return 0;
}
