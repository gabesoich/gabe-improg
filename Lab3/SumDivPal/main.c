/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 18/09/2018 */
/* version: 0.0.1 */
/* Description: Sums an integers proper divisors, and finds whether or not 
that sum is the reverse of the number */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int sumProperDivisors(int num) {
    int
        sum = 0,
        half_num = num / 2;
    for (int i = 1; i <= half_num; i++) {
        if (num % i == 0) {
            sum += i;
        }
    }
    return sum;
}

int numLength(int num) {
    return (num > 999999999)
        ? 10 : (num > 99999999)
        ? 9 : (num > 9999999)
        ? 8 : (num > 999999)
        ? 7 : (num > 99999)
        ? 6 : (num > 9999)
        ? 5 : (num > 999)
        ? 4 : (num > 99)
        ? 3 : (num > 9)
        ? 2 : (num > 0)
        ? 1 : 0;
}

int getDigit(int num, int digit) {
    for (int i = digit; i > 1; i--) {
        num /= 10;
    }
    return num % 10;
}

bool isPalindrome(int num1, int num2) {
    bool is_pal = false;
    int len1 = numLength(num1);
    int len2 = numLength(num2);
    if (len1 != len2) {
        // if they're not the same length they're not palindromes
        return is_pal;
    }
    for (int j = len1; j > 0; j--) {
        if (getDigit(num1, j) != num2 % 10) {
            break;
        } else if (j == 1) {
            is_pal = true;
        }
        num2 /= 10;
    }
    return is_pal;
}

int main(int argc, char *argv[]) {

    int given_integer;
    scanf("%d", &given_integer);

    if (isPalindrome(given_integer, sumProperDivisors(given_integer))) {
        printf("YES");
    } else {
        printf("NO");
    }

    return 0;
}
