/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 11/09/2018 */
/* version: 0.0.1 */
/* Description: finds the smallest k composite for a
    given integer between 1 and 22 inclusive */

#include <stdio.h>
#include <stdlib.h>


int gcd(int a, int b) {
    int largest = a >= b ? a : b;
    int smallest = a < b ? a : b;
    int rem = largest % smallest;
    if (rem == 0) {
        return smallest;
    } else {
        return gcd(smallest, rem);
    }
}

int lcm(int a, int b) {
    return a / gcd(a, b) * b;
}

int main(int argc, char *argv[]) {
    int k;
    scanf("%d", &k);
    
    int k_comp = 1;
    for (int i = 1; i <= k; i++) {
        k_comp = lcm(i, k_comp);
    }

    printf("%d", k_comp);

    return 0;
}
