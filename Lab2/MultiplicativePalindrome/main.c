/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 12/09/2018 */
/* version: 0.0.1 */
/* Description: finds the number of mulpals between 2 integers */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int numLength(int num) {
    return (num > 999999999)
        ? 10 : (num > 99999999)
        ? 9 : (num > 9999999)
        ? 8 : (num > 999999)
        ? 7 : (num > 99999)
        ? 6 : (num > 9999)
        ? 5 : (num > 999)
        ? 4 : (num > 99)
        ? 3 : (num > 9)
        ? 2 : (num > 0)
        ? 1 : 0;
}

int getDigit(int num, int digit) {
    int a = num;
    for (int i = digit; i > 1; i--) {
        a /= 10;
    }
    return a % 10;
}

bool isMulpal(int num) {
    bool is_mulpal = false;    
    int number_to_check;
    for (int i = 1; i < 10; i++) {
        if (is_mulpal) {
            break;
        }
        number_to_check = num * i;
        int len = numLength(number_to_check);
        if (len != numLength(num)) {
            continue;
        }
        for (int j = len; j > 0; j--) {
            if (getDigit(num, j) != number_to_check % 10) {
                break;
            } else if (j == 1) {
                is_mulpal = true;
            }
            number_to_check /= 10;
        }
    }
    return is_mulpal;
}

int main(int argc, char *argv[])
{
    int
        a,
        b,
        num_mulpals = 0;

    scanf("%d %d", &a, &b);
    for (int i = a; i <= b; i++) {
        if (i < 100) {
            if (i % 11 == 0) {
                num_mulpals++;
            } else if (i / 10 == 0) {
                num_mulpals++;
            }
        } else if (isMulpal(i)) {
            num_mulpals++;
        }
    }

    printf("%d", num_mulpals);

    return 0;
}
