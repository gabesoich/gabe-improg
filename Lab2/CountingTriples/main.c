/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 13/09/2018 */
/* version: 0.0.1 */
/* Description: finds the number of integer triples whose product
    equals a given integer */

#include <stdio.h>
#include <stdlib.h>

int squareRoot(long int num) {
    if (num == 0 || num == 1) {
        return num;
    }
    long int i = 1, result = 1;
    while (result <= num) {
        i++;
        result = i * i;
    }
    return (int)(i - 1);
}

int cubeRoot(long int num) {
    if (num == 0 || num == 1) {
        return num;
    }
    long int i = 1, result = 1;
    while (result <= num) {
        i++;
        result = i * i * i;
    }
    return (int)(i - 1);
}

int main(int argc, char *argv[])
{
    long int number;
    scanf("%ld", &number);

    int num_triples = 0;

    long int
        num1,
        num2,
        num3;

    // declared here for clarity, used to avoid recomputing inside the loop
    long int number_over_num1;

    int
        square_root = squareRoot(number),
        cube_root = cubeRoot(number);

    // calculate the triplets only in ascending order, avoiding duplicates
    for (num1 = 1; num1 <= cube_root; num1++) {
        // skip if not a factor
        if (number % num1 != 0) {
            continue;
        }
        // to avoid recomputing inside the inner loop
        number_over_num1 = number / num1;
        for (num2 = num1; num2 <= square_root; num2++) {
            // again, skip if not a factor
            if (number % num2 != 0) {
                continue;
            }
            num3 = number_over_num1 / num2;
            // only ascending order allowed
            if (num3 < num2) {
                continue;
            }
            if (num1 * num2 * num3 == number) {
                num_triples++;
            }
        }
    }

    printf("%d", num_triples);
    return 0;
}
