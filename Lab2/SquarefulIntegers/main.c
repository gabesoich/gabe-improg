/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 10/09/2018 */
/* version: 0.0.1 */
/* Description: determines whether or not a given number is squareful */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

double squareRoot(double num) {
    double a = num;
    double b = 1;
    double approx_min_diff = 0.00000001;
    while(a - b > approx_min_diff) {
        a = (a + b) / 2;
        b = num / a;
    }
    return a;
}

bool isSquareFree(int n) {
    int num = n % 2 == 0 ? n / 2 : n;
    if (num % 2 == 0) {
        // square free if not divisble by fact more than once
        return false; 
    }
    int root_num = (int)squareRoot(n);
    // can skip all even as multiples of 2
    for (int i = 3; i < root_num; i += 2) {
        if (num % i == 0) {
            num = num / i;
        }
        if (num % i == 0) {
            // as above, can't have 2 same factor
            return false;
        }
    }
    return true;
}

void printNotSquareful(double number) {
    printf("%d is not a squareful number.", (int)number);
}
void printSquareful(double number) {
    printf("%d is a squareful number.", (int)number);
}

int main(int argc, char *argv[]) {
    double number;
    scanf("%lf", &number);
    
    double square_root = squareRoot(number);

    // if there is no integer square root
    if (square_root - (int)square_root > 0.0001) {
        printNotSquareful(number);
    } else {
        int int_square_root = (int)square_root;

        if (isSquareFree(int_square_root)) {
            printSquareful(number);
        } else {
            printNotSquareful(number);
        }
    }


    return 0;
}
