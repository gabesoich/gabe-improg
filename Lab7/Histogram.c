/* file: Histogram.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 16/10/2018 */
/* version: 0.0.1 */
/* Description: Outputs a list of integers in ascending order,
    with their frequency as given by an input list */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void *safeMalloc(size_t size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeCalloc(size_t size) {
    void *p = calloc(size, size);
    if (p == NULL) {
        printf("No more memory? calloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeRealloc(void *p, size_t size) {
    void *temp = realloc(p, size);
    if (temp == NULL) {
        printf("No more memory? realloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return temp;
}


typedef struct IntFreq {
    int num;
    int freq;
} IntFreq_t;

void printArr(IntFreq_t *arr, int len) {
    for (int i = 0; i < len; i++) {
        printf("(%d: %d) ", arr[i].num, arr[i].freq);
    }
    printf("\n");
}

typedef struct FreqSet {
    IntFreq_t *frequencies;
    int current_max;
    int current_size;
} FreqSet_t;

// functions for FreqSet
void initFreqSet(FreqSet_t *fs, int initial_size) {
    fs->frequencies = safeMalloc(initial_size * sizeof(IntFreq_t));
    // init all IntFreq to 0, num to -1 to indicate no num
    for (int i = 0; i < initial_size; i++) {
        IntFreq_t *curr = &fs->frequencies[i];
        curr->freq = 0;
        curr->num = -1;
    }
    fs->current_max = initial_size;
    fs->current_size = 0;
}

int findIndex(IntFreq_t *freqs, int left_idx, int right_idx, int value) {
    if (right_idx >= left_idx) {
        int middle_idx = left_idx + (right_idx - left_idx) / 2;
        if (freqs[middle_idx].num == value) {
            return middle_idx;
        } else if (
            freqs[middle_idx].num > value || 
            freqs[middle_idx].num == -1
        ) {
            return findIndex(freqs, left_idx, middle_idx - 1, value);
        } else {
            return findIndex(freqs, middle_idx + 1, right_idx, value);
        }
    } else {
        return -1;
    }
}

void addItem(FreqSet_t *fs, int x) {
    // resize if necessary
    
    if (fs->current_size == fs->current_max) {

        int prev_max = fs->current_max;
        fs->current_max *= 2;

        fs->frequencies = safeRealloc(fs->frequencies, fs->current_max * sizeof(IntFreq_t));

        IntFreq_t *ifs = fs->frequencies;
        // as in initFreqSet
        for (int i = prev_max + 1; i < fs->current_max; i++) {
            ifs[i].freq = 0;
            ifs[i].num = -1;
        }
    }
    IntFreq_t *ifs = fs->frequencies;

    // int index = findIndex(ifs, 0, fs->current_max - 1, x);

    ifs[fs->current_size].num = x;
    ifs[fs->current_size].freq = 1;
    fs->current_size++;
}

void swap(IntFreq_t *a, IntFreq_t *b) {
    IntFreq_t temp;
    temp.freq = a->freq;
    temp.num = a->num;
    a->freq = b->freq;
    a->num = b->num;
    b->freq = temp.freq;
    b->num = temp.num;
}

int getQsPivot(FreqSet_t *fs, int low_idx, int high_idx) {
    int pivot_num = fs->frequencies[high_idx].num;
    int smallest_el_idx = low_idx - 1;
    for (int i = low_idx; i <= high_idx - 1; i++) {
        if (fs->frequencies[i].num <= pivot_num) {
            smallest_el_idx++;
            swap(fs->frequencies + smallest_el_idx, fs->frequencies + i);
        }
    }
    swap(fs->frequencies + smallest_el_idx + 1, fs->frequencies + high_idx);
    return smallest_el_idx + 1;
}
void quickSort(FreqSet_t *fs, int low_idx, int high_idx) {
    if (low_idx < high_idx) {
        int pivot_idx = getQsPivot(fs, low_idx, high_idx);
        quickSort(fs, low_idx, pivot_idx - 1);
        quickSort(fs, pivot_idx + 1, high_idx);
    }
}

void sortFreqSet(FreqSet_t *fs) {
    quickSort(fs, 0, fs->current_size - 1);
}

void freeFreqSet(FreqSet_t *fs) {
    free(fs->frequencies);
    fs->frequencies = NULL;
    fs->current_max = 0;
    fs->current_size = 0;
}
// end functions for FreqSet

int main(int argc, char *argv[]) {
    int current_number;
    
    FreqSet_t nums;
    initFreqSet(&nums, 8);

    while(true) {
        scanf("%d", &current_number);
        if (current_number == 0) {
            break;
        } else {
            addItem(&nums, current_number);
        }
    }

    sortFreqSet(&nums);

    IntFreq_t prev = nums.frequencies[0];
    for (int i = 1; i < nums.current_size; i++) {
        IntFreq_t curr = nums.frequencies[i];
        if (prev.num == curr.num) {
            prev.freq += curr.freq;
        } else {
            printf("%d: %d\n", prev.num, prev.freq);
            prev = curr;
        }
    }
    printf("%d: %d\n", prev.num, prev.freq);

    freeFreqSet(&nums);
    return 0;
}