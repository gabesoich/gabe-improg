/* file: PlacingCoins.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 20/10/2018 */
/* version: 0.0.1 */
/* Description: Finds the maximal inter coin distance for a 
    given set of distances */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void *safeMalloc(size_t size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeCalloc(size_t size) {
    void *p = calloc(size, size);
    if (p == NULL) {
        printf("No more memory? calloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeRealloc(void *p, size_t size) {
    void *temp = realloc(p, size);
    if (temp == NULL) {
        printf("No more memory? realloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return temp;
}

typedef struct IntSet {
    int *numbers;
    int current_max;
    int current_size;
} IntSet_t;

// functions for IntSet
void initIntSet(IntSet_t *is, int initial_size) {
    is->numbers = safeMalloc(initial_size * sizeof(int));
    is->current_size = 0;
    is->current_max = initial_size;
}

void printIntSet(IntSet_t is) {
    for (int i = 0; i < is.current_max; i++) {
        printf("%d ", is.numbers[i]);
    }
    printf("\n");
}

void addItem(IntSet_t *is, int x) {
    // resize if necessary
    if (is->current_size == is->current_max) {
        is->current_max *= 2;

        is->numbers = safeRealloc(is->numbers, is->current_max * sizeof(int));
    }
    is->numbers[is->current_size++] = x;
}

void swap(int *a, int *b) {
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

int getQsPivot(IntSet_t *is, int low_idx, int high_idx) {
    int pivot_num = is->numbers[high_idx];
    int smallest_el_idx = low_idx - 1;
    for (int i = low_idx; i <= high_idx - 1; i++) {
        if (is->numbers[i] <= pivot_num) {
            smallest_el_idx++;
            swap(is->numbers + smallest_el_idx, is->numbers + i);
        }
    }
    swap(is->numbers + smallest_el_idx + 1, is->numbers + high_idx);
    return smallest_el_idx + 1;
}
void quickSort(IntSet_t *is, int low_idx, int high_idx) {
    if (low_idx < high_idx) {
        int pivot_idx = getQsPivot(is, low_idx, high_idx);
        quickSort(is, low_idx, pivot_idx - 1);
        quickSort(is, pivot_idx + 1, high_idx);
    }
}

void sortIntSet(IntSet_t *is) {
    quickSort(is, 0, is->current_size - 1);
}

void freeIntSet(IntSet_t *is) {
    free(is->numbers);
    is->numbers = NULL;
    is->current_max = 0;
    is->current_size = 0;
}
// end functions for IntSet

bool canPlaceAllOtherCoins(IntSet_t points, int min_distance, int current_index, int number_of_coins) {
    if (number_of_coins == 0) {
        return true;
    } else {
        int i = current_index + 1;
        int *n = points.numbers;
        while (i != points.current_size) {
            // the distance between these 2 is big enough, check the rest
            if (n[i] - n[current_index] >= min_distance) {
                return canPlaceAllOtherCoins(points, min_distance, i, number_of_coins - 1);
            } else {
                i++;
            }
        }
        // couldn't place this one
        return false;
    }
}

int largestPossibleDistance(IntSet_t points, int lower, int upper, int number_of_coins) {
        int middle_dist = lower + (upper - lower) / 2;
        if (lower == upper - 1) {
            return canPlaceAllOtherCoins(points, upper, 0, number_of_coins) ? upper : lower;
        } else if (canPlaceAllOtherCoins(points, middle_dist, 0, number_of_coins)) {
            return largestPossibleDistance(points, middle_dist, upper, number_of_coins);
        } else {
            return largestPossibleDistance(points, lower, middle_dist, number_of_coins);
        }
}

int findMaxDistance(IntSet_t points, int number_of_coins) {
    // for convenience
    int *n = points.numbers;
    int
        // 1 at first index
        coins_left_to_place = number_of_coins - 1,
        // largest possible is n_rem_coins away from the end of dists 
        largest_theoretical_distance =  n[points.current_size - coins_left_to_place] - n[0];

    if (coins_left_to_place == 0) {
        return largest_theoretical_distance;
    } else {
        return largestPossibleDistance(points, 1, largest_theoretical_distance, coins_left_to_place);
    }
}

int main(int argc, char *argv[]) {
    int 
        number_of_coins,
        number_of_distances;
    scanf("%d %d", &number_of_coins, &number_of_distances);

    IntSet_t distances;
    initIntSet(&distances, 8);

    for (int i = 0; i < number_of_distances; i++) {
        int curr;
        scanf("%d", &curr);
        addItem(&distances, curr);
    }

    sortIntSet(&distances);

    printf("%d", findMaxDistance(distances, number_of_coins));

    freeIntSet(&distances);
    return 0;
}