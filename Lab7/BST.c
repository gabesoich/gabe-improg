/* file: BST.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 17/10/2018 */
/* version: 0.0.1 */
/* Description: Generates a text representation of a given BST */

#include <stdio.h>
#include <stdlib.h>

void *safeMalloc(size_t size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeCalloc(size_t size) {
    void *p = calloc(size, size);
    if (p == NULL) {
        printf("No more memory? calloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeRealloc(void *p, size_t size) {
    void *temp = realloc(p, size);
    if (temp == NULL) {
        printf("No more memory? realloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return temp;
}

typedef struct IntSet {
    int *numbers;
    int current_max;
    int current_size;
} IntSet_t;

// functions for IntSet
void initIntSet(IntSet_t *is, int initial_size) {
    is->numbers = safeMalloc(initial_size * sizeof(int));
    is->current_size = 0;
    is->current_max = initial_size;
}

void printIntSet(IntSet_t is) {
    for (int i = 0; i < is.current_max; i++) {
        printf("%d ", is.numbers[i]);
    }
    printf("\n");
}

void addItem(IntSet_t *is, int x) {
    // resize if necessary
    if (is->current_size == is->current_max) {
        is->current_max *= 2;

        is->numbers = safeRealloc(is->numbers, is->current_max * sizeof(int));
    }
    is->numbers[is->current_size++] = x;
}

void swap(int *a, int *b) {
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

int getQsPivot(IntSet_t *is, int low_idx, int high_idx) {
    int pivot_num = is->numbers[high_idx];
    int smallest_el_idx = low_idx - 1;
    for (int i = low_idx; i <= high_idx - 1; i++) {
        if (is->numbers[i] <= pivot_num) {
            smallest_el_idx++;
            swap(is->numbers + smallest_el_idx, is->numbers + i);
        }
    }
    swap(is->numbers + smallest_el_idx + 1, is->numbers + high_idx);
    return smallest_el_idx + 1;
}
void quickSort(IntSet_t *is, int low_idx, int high_idx) {
    if (low_idx < high_idx) {
        int pivot_idx = getQsPivot(is, low_idx, high_idx);
        quickSort(is, low_idx, pivot_idx - 1);
        quickSort(is, pivot_idx + 1, high_idx);
    }
}

void sortIntSet(IntSet_t *is) {
    quickSort(is, 0, is->current_size - 1);
}

void freeIntSet(IntSet_t *is) {
    free(is->numbers);
    is->numbers = NULL;
    is->current_max = 0;
    is->current_size = 0;
}
// end functions for IntSet

void printTree(int index, IntSet_t nums, int level, int max_depth) {
    int this_num = nums.numbers[index];
    if (level != 0) {
        printf("(");
    }
    if (level == max_depth - 1) {
        printf("Leaf %d", this_num);
    } else {
        int
            next_level = level + 1,
            index_jump = 1 << (max_depth - next_level - 1);
        printf("Tree ");
        printTree(index - index_jump, nums, next_level, max_depth);
        printf(" %d ", this_num);
        printTree(index + index_jump, nums, next_level, max_depth);
    }
    if (level != 0) {
        printf(")");
    }
}

int main(int argc, char *argv[]) {
    int depth;
    scanf("%d", &depth);

    IntSet_t nums;
    initIntSet(&nums, 8);

    int num_els = (1 << depth) - 1;

    for (int i = 0; i < num_els; i++) {
        int curr;
        scanf("%d", &curr);
        addItem(&nums, curr);
    }

    sortIntSet(&nums);
    int root_index = num_els / 2;
    printTree(root_index, nums, 0, depth);

    freeIntSet(&nums);
    return 0;
}