/* file: FreqCount.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 16/10/2018 */
/* version: 0.0.1 */
/* Description: Counts the frequency of integers in a list, and outputs
    the winner if any, and the second place (if any) if there is a winner */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void *safeMalloc(size_t size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeCalloc(size_t size) {
    void *p = calloc(size, size);
    if (p == NULL) {
        printf("No more memory? calloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeRealloc(void *p, size_t size) {
    void *temp = realloc(p, size);
    if (temp == NULL) {
        printf("No more memory? realloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return temp;
}


typedef struct IntFreq {
    int num;
    int freq;
} IntFreq_t;

void printArr(IntFreq_t *arr, int len) {
    for (int i = 0; i < len; i++) {
        printf("(%d: %d) ", arr[i].num, arr[i].freq);
    }
    printf("\n");
}

typedef struct FreqSet {
    IntFreq_t *frequencies;
    int current_max;
    int current_size;
    int num_items;
} FreqSet_t;

// functions for FreqSet
void initFreqSet(FreqSet_t *fs, int initial_size) {
    fs->frequencies = safeMalloc(initial_size * sizeof(IntFreq_t));
    // init all IntFreq to 0, num to -1 to indicate no num
    for (int i = 0; i < initial_size; i++) {
        IntFreq_t *curr = &fs->frequencies[i];
        curr->freq = 0;
        curr->num = -1;
    }
    fs->current_max = initial_size;
    fs->current_size = 0;
    fs->num_items = 0;
}

void addItem(FreqSet_t *fs, int x) {
    // resize if necessary
    
    if (fs->current_size == fs->current_max) {

        int prev_max = fs->current_max;
        fs->current_max *= 2;

        fs->frequencies = safeRealloc(fs->frequencies, fs->current_max * sizeof(IntFreq_t));

        IntFreq_t *ifs = fs->frequencies;
        // as in initFreqSet
        for (int i = prev_max + 1; i < fs->current_max; i++) {
            ifs[i].freq = 0;
            ifs[i].num = -1;
        }
    }
    IntFreq_t *ifs = fs->frequencies;
    // find the number, or add it if not found.
    // Increment freq by 1
    for (int i = 0; i < fs->current_max; i++) {
        if (ifs[i].num == -1) {
            fs->current_size++;
            ifs[i].num = x;
            ifs[i].freq = 1;
            break;
        } else if (ifs[i].num == x) {
            ifs[i].freq += 1;
            break;
        }
    }
    fs->num_items++;
}

void freeFreqSet(FreqSet_t *fs) {
    free(fs->frequencies);
    fs->frequencies = NULL;
    fs->current_max = 0;
    fs->current_size = 0;
    fs->num_items = 0;
}
// end functions for FreqSet

void findMajorityAndRunnerUp(int res[2], FreqSet_t nums) {
    int
        majority = -1,
        runner_up = -1;
    for (int i = 0; i < nums.current_size; i++) {
        IntFreq_t curr = nums.frequencies[i];
        double proportion = (double)curr.freq / (double)nums.num_items * 100;
        if (proportion > 50) {
            majority = curr.num;
        } else if (proportion > 25) {
            runner_up = curr.num;
        }

        if (majority != -1 && runner_up != -1) {
            break;
        }
    }
    res[0] = majority;
    res[1] = runner_up;
}

int main(int argc, char *argv[]) {
    int current_number;
    
    FreqSet_t nums;
    initFreqSet(&nums, 8);

    while(true) {
        scanf("%d", &current_number);
        if (current_number == 0) {
            break;
        } else {
            addItem(&nums, current_number);
        }
    }

    int results[2];
    findMajorityAndRunnerUp(results, nums);
    int
        majority = results[0],
        runner_up = results[1];

    if (majority != -1) {
        printf("majority: %d", majority);
        if (runner_up != -1) {
            printf("\nrunner-up: %d", runner_up);
        } else {
            printf("\nrunner-up: NONE");
        }
    } else {
        printf("majority: NONE");
    }

    freeFreqSet(&nums);
    return 0;
}