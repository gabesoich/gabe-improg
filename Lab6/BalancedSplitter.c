/* file: BalancedSplitter.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 08/10/2018 */
/* version: 0.0.1 */
/* Description: Finds the smallest integer n that evenly splits the total 
    frequency count of ints in a list above and below n */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void *safeMalloc(size_t size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeCalloc(size_t size) {
    void *p = calloc(size, size);
    if (p == NULL) {
        printf("No more memory? calloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

// each iteration, holdss
// the number of ints below n
// equal to n
// above n
typedef struct Count {
    int lower;
    int equal;
    int upper;
} Count;

int main(int argc, char *argv[]) {

    int
        smallest_splitter = -1,
        highest_value = 0;

    int *number_frequency = safeCalloc(100 * sizeof(int));
    Count *counts = safeMalloc(100 * sizeof(Count));

    int current_val;
    while(true) {
        scanf("%d", &current_val);
        if (current_val == 0) {
            break;
        }
        highest_value = current_val > highest_value
            ? current_val
            : highest_value;
        number_frequency[current_val - 1]++;
    }
    
    // set vals for int 1
    counts[0].lower = 0;
    counts[0].equal = number_frequency[0];
    counts[0].upper = 0;
    for (int i = 2; i <= highest_value; i++) {
        counts[0].upper += number_frequency[i - 1];
    }
    
    for (int i = 1; i < highest_value; i++) {
        int current_int = i + 1;

        // qol
        Count *p = &counts[i-1];
        Count *c = &counts[i];

        c->lower = p->lower + p->equal;
        // number freq[0] is for int 1
        c->equal = number_frequency[i];
        c->upper = p-> upper - c->equal;

        if (c->lower == c->upper) {
            smallest_splitter = current_int;
            break;
        }
    }

    if (smallest_splitter == -1) {
        printf("UNBALANCED");
    } else {
        printf("%d", smallest_splitter);
    }

    free(number_frequency);
    free(counts);
    return 0;
}
