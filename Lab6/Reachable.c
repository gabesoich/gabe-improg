/* file: Reachable.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 11/10/2018 */
/* version: 0.0.1 */
/* Description: Finds whether we can reach the last index of an int[],
    jumping up to current_int distances at a time */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void *safeMalloc(size_t size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

// too slow, checks every possible path
bool canReachLastIndex(int current_index, const int *array, int array_size) {
    if (current_index == array_size - 1) {
        return true;
    } else {
        bool can_reach = false;
        for (int i = array[current_index]; i > 0; i--) {
            int new_idx = current_index + i;
            if (new_idx < array_size) {
                can_reach = can_reach || canReachLastIndex(new_idx, array, array_size);
            }
        }
        return can_reach;
    }
}

bool canReachLastIndexReversed(int current_index, const int *array, int array_size) {
    if (current_index == 0) {
        return true;
    } else {
        bool can_reach = false;
        for (int min_size = 1; min_size < array_size; min_size++) {
            int idx = current_index - min_size;
            
            if (array[idx] >= min_size) {
                can_reach = canReachLastIndexReversed(idx, array, array_size);
                break;
            }
        }
        return can_reach;
    }
}

int main(int argc, char *argv[]) {
    
    int array_length;
    scanf("%d", &array_length);

    int *jumps = safeMalloc(array_length * sizeof(int));
    for (int i = 0; i < array_length; i++) {
        scanf("%d", &jumps[i]);
    }

    if (canReachLastIndexReversed(array_length - 1, jumps, array_length)) {
        printf("YES");
    } else {
        printf("NO");
    }

    free(jumps);

    return 0;
}
