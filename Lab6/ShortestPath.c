/* file: ShortestPath.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 09/10/2018 */
/* version: 0.0.1 */
/* Description: Finds the shortest path from n to 1, following certain rules */

#include <stdio.h>
#include <stdlib.h>

int numberOfSteps(int n) {
    if (n == 1) {
        return 0;
    } else if (n % 2 == 0) {
        return 1 + numberOfSteps(n / 2);
    } else {
        int one_up = 1 + numberOfSteps(n + 1);
        int one_down = 1 + numberOfSteps(n - 1);
        return one_down > one_up ? one_up : one_down;
    }
}

int main(int argc, char *argv[]) {
    
    int n;
    scanf("%d", &n);

    printf("%d", numberOfSteps(n));

    return 0;
}
