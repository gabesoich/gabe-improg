/* file: PangramPrimes.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 09/10/2018 */
/* version: 0.0.1 */
/* Description: Finds the number of pangram primes between two integers */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

void *safeMalloc(size_t size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeCalloc(size_t size) {
    void *p = calloc(size, size);
    if (p == NULL) {
        printf("No more memory? calloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeRealloc(void *p, size_t size) {
    void *temp = realloc(p, size);
    if (temp == NULL) {
        printf("No more memory? realloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return temp;
}

// convenient state holder
typedef struct PangramData {
    int number_of_pangrams;
    int highest_number;
    int upper_bound;
    int lowest_number;
    int lower_bound;
} PangramData_t;

void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

int highestPossibleInteger(int num) {
    return (num > 99999999)
        ? 9 : (num > 9999999)
        ? 8 : (num > 999999)
        ? 7 : (num > 99999)
        ? 6 : (num > 9999)
        ? 5 : (num > 999)
        ? 4 : (num > 99)
        ? 3 : (num > 9)
        ? 2 : (num > 0)
        ? 1 : 0;
}

bool isPrime(int num) {
    if (num <= 1) {
        return false;
    } else if (num <= 3) {
        return true;
    } else if (num % 2 == 0 || num % 3 == 0) {
        return false;
    }
    // uses (6k + 1)<=sqrt(num) to test primality
    // for numbers > 3
    int i = 5;
    while (i * i <= num) {
        if (num % i == 0 || num % (i + 2) == 0) {
            return false;
        }
        i += 6;
    }
    return true;
}

bool isPangram(int num) {
    int digit_freq_count[9] = {0};
    while (num > 0) {
        int num_idx = num % 10 - 1;
        if (num_idx == -1) {
            return false;
        } else {
            digit_freq_count[num_idx] += 1;
            if (digit_freq_count[num_idx] > 1) {
                return false;
            }
        }
        num /= 10;
    }
    bool is_pangram = true;
    for (int i = 0; i < 8; i++) {
        if (digit_freq_count[i + 1] && !digit_freq_count[i]) {
            is_pangram = false;
            break;
        }
    }
    return is_pangram;
}

void printArr(int *arr, int len) {
    for (int i = 0; i < len; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void findPangramPrimes(
        int **numbers,
        int current_size,
        int orig_size,
        PangramData_t *p
    ) {
    int *nums = *numbers;
    if (current_size == 1) {
        int perm = 0;
        for (int i = 0; i < orig_size; i++) {
            perm += nums[i] * (int)pow(10, (double)i);
        }
        if (perm <= p->upper_bound && perm >= p->lower_bound) {
            if (isPrime(perm)) {
                p->number_of_pangrams++;
            }
        }
        return;
    }
    for (int i = 0; i < current_size; i++) {
        findPangramPrimes(numbers, current_size - 1, orig_size, p);
        if (current_size % 2 != 0) {
            swap(&nums[0], &nums[current_size - 1]);
        } else {
            swap(&nums[i], &nums[current_size - 1]);
        }
    }
}

int main(int argc, char *argv[]) {

    PangramData_t p;
    int *numbers = safeCalloc(9 * sizeof(int));

    scanf("%d", &p.lower_bound);
    scanf("%d", &p.upper_bound);

    p.number_of_pangrams = 0;
    p.highest_number = highestPossibleInteger(p.upper_bound);
    p.lowest_number = highestPossibleInteger(p.lower_bound);

    if (p.highest_number == p.lowest_number) {
        p.lowest_number--;
    }
    
    for (int i = p.highest_number; i > p.lowest_number; i--) {
        for (int i = 0; i < p.highest_number; i++) {
            numbers[i] = i + 1;
        }
        findPangramPrimes(&numbers, i, i, &p);
    }

    printf("%d", p.number_of_pangrams);

    free(numbers);
    return 0;
}
