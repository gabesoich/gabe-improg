/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 25/09/2018 */
/* version: 0.0.1 */

#include <stdio.h>
#include <stdlib.h>

int fibTotal(int iters) {
    int
        a = 0,
        b = 1,
        temp,
        current_total = 1,
        num_dead_rabbits;
    for (int i = 1; i <= iters; i++) {
        
        num_dead_rabbits = i >= 5 ? fibTotal(i - 5) : 0;
        printf("%d ded\n", num_dead_rabbits);
        temp = a;
        a = b;
        b += temp - num_dead_rabbits;
        current_total = b;
    }
    return current_total;
}

int main(int argc, char *argv[]) {
    int year;

    scanf("%d", &year);

    printf("%d", fibTotal(year));
    return 0;
}
