/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 25/09/2018 */
/* version: 0.0.1 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int
        num1,
        num2,
        ans;
    
    char op;
    
    if (scanf("%d", &num1)) {
        scanf("%c", &op);
        if (scanf("%d", &num2)) {
            // x at end
            ans = op == '+' ? num1+num2 : num1 - num2;
        } else {
            scanf("x=%d", &num2);
            // x in middle
            ans = op == '+' ? num2 - num1 : num1 - num2;
        }
    } else {
        // x at start
        scanf("%*c%c", &op);
        scanf("%d=%d", &num1, &num2);
        ans = op == '+' ? num2 - num1 : num1 + num2;
    }

    printf("x=%d", ans);    

    return 0;
}