/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 25/09/2018 */
/* version: 0.0.1 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int sumProperDivisors(int num) {
    int
        sum = 0,
        half_num = num / 2;
    for (int i = 1; i <= half_num; i++) {
        if (num % i == 0) {
            sum += i;
        }
    }
    return sum;
}

int main(int argc, char *argv[]) {
    int
        num1,
        num2;
    
    scanf("%d %d", &num1, &num2);

    if (sumProperDivisors(num1) == num2 && sumProperDivisors(num2) == num1) {
        printf("YES");
    } else {
        printf("NO");
    }
    

    return 0;
}