/* file: main.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 25/09/2018 */
/* version: 0.0.1 */

#include <stdio.h>
#include <stdlib.h>

int hcpf(int num1, int num2) {
    int 
        smallest = num1 < num2 ? num1 : num2,
        highest_common_prime_factor = 1;

    for (int i = 2; i <= smallest; i++) {
        if (num1 % i == 0 && num2 % i == 0) {
            highest_common_prime_factor = i;
        }
        while (num1 % i == 0) {
            num1 /= i;
        }
        while (num2 % i == 0) {
            num2 /= i;
        }
    }

    return highest_common_prime_factor;
}

int main(int argc, char *argv[]) {
    int
        num1,
        num2;

    scanf("%d %d", &num1, &num2);

    printf("%d", hcpf(num1, num2));

    return 0;
}