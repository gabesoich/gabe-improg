#include <stdio.h>
#include <stdlib.h>
// forward back 4 7 2 3 3 5 19 19 0

int branchWorks(int series[], int length, int index, int visited_index[]) {
    int jump_distance = series[index];
    if (jump_distance == 0) return 1;
    int
        new_left_index = index - jump_distance,
        new_right_index = index + jump_distance,
        can_jump_left = new_left_index > 0 && !visited_index[new_left_index],
        can_jump_right = new_right_index < length && !visited_index[new_right_index];

    visited_index[index] = 1;
    if (can_jump_right && branchWorks(series, length, new_right_index, visited_index)) {
        return 1;
    } else if (can_jump_left && branchWorks(series, length, new_left_index, visited_index)) {
        return 1;
    } else {
        return 0;
    }
}

int isSolvable(int len, int series[]) {
    int visited_indexes[20] = {0};
    return branchWorks(series, len, series[0], visited_indexes);
}

int main(int argc, char *argv[]) {
    int len=0, series[20];
    do {
        scanf("%d", &series[len]);
        len++;
    } while (series[len-1] != 0);
    printf("%d\n", isSolvable(len, series));
    return 0;
}