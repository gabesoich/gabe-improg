#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void *safeMalloc(size_t size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void initString(char *string, int length) {
    for (int i = 0; i < length; i++) {
        string[i] = '\0';
    }
}

int readString(char *string) {
    int index = 0;
    while(true) {
        char current;
        scanf("%c", &current);
        if (current == '.') {
            break;
        }
        string[index++] = current;
    }
    return index;
}

int findM(int k) {
    int m = 1;
    while (m*m <= k) {
        m++;
    }
    return --m;
}

int findN(int k, int m) {
    int n = 1;
    while (m*n < k) {
        n++;
    }
    return n;
}

int** createMatrix(int m, int n) {
    int **matrix = safeMalloc(m * sizeof(int *));
    for (int i = 0; i < m; i++) {
        matrix[i] = safeMalloc(n * sizeof(int));
    }
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            matrix[i][j] = '#';
        }
    }
    return matrix;
}

void fillMatrix(int **matrix, char *string, int k, int n) {
    for (int i = 0; i < k; i++) {
        int
            row_idx = i / n,
            column_idx = i % n;
        if (string[i] != ' ') {
            matrix[row_idx][column_idx] = string[i];
        }
    }
}

void printMatrix(int **matrix, int m, int n) {
    for (int i = 0; i < m * n; i++) {
        int
            column_idx = i / m,
            row_idx = i % m;
        printf("%c", matrix[row_idx][column_idx]);
    }
    printf("\n");
}

void freeMatrix(int **matrix, int m) {
    for (int i = 0; i < m; i++) {
        free(matrix[i]);
    }
    free(matrix);
}

int main(int argc, char *argv[]) {
    char string[401];
    initString(string, 401);

    const int
        k = readString(string),
        m = findM(k),
        n = findN(k, m);

    int **matrix = createMatrix(m, n);

    fillMatrix(matrix, string, k, n);

    printMatrix(matrix, m, n);

    freeMatrix(matrix, m);
    return 0;
}