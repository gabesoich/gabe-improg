#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void *safeMalloc(size_t size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

int main(int argc, char *argv[]) {
    int n;
    scanf("%d", &n);

    bool *present = safeMalloc((n + 1) * sizeof(bool));

    for (int i = 0; i <= n; i++) {
        present[i] = false;
    }

    for (int i = 0; i < n; i++) {
        int number;
        scanf("%d", &number);
        present[number] = true;
    }

    for (int i = 0; i <= n; i++) {
        if (present[i] == false) {
            printf("%d", i);
            break;
        }
    }

    free(present);

    return 0;
}