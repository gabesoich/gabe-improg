/* file: recurrence.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 02/10/2018 */
/* version: 0.0.1 */
/* Description: computes the answer of F_n(i) for given integers i and n */

#include <stdio.h>
#include <stdlib.h>

void *safeMalloc(int size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%d) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}


int F(int n, int i, int *prev_vals) {
    if (i < n) {
        return i;
    } else {
        int result = 0;
        for (int j = 1; j <= n; j++) {
            // (-1)^(j+1)
            int neg_multiplier = j % 2 == 0 ? -1 : 1;
            int x = neg_multiplier * j * prev_vals[i - j];
            result += x;
        }
        return result;
    }
}

int main(int argc, char *argv[]) {
    int n, i;
    scanf("%d %d", &n, &i);

    int *vals = safeMalloc((i + 1) * sizeof(int));

    for (int k = 0; k <= i; k++) {
        vals[k] = F(n, k, vals);
    }

    printf("%d", vals[i]);

    free(vals);
    return 0;
}
