/* file: swapping.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 02/10/2018 */
/* version: 0.0.1 */
/* Description: decides whether an array of N integers is in ascending
    order after multiple swaps on elements */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void *safeMalloc(int size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%d) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

int main(int argc, char *argv[]) {

    int list_size;
    scanf("%d", &list_size);

    int *list = safeMalloc(list_size*sizeof(int));

    for(int i = 0; i < list_size; i++) {
        scanf("%d", &list[i]);
    }

    int pos1, pos2, temp;
    scanf("%d %d", &pos1, &pos2);
    while(!(pos1 == 0 && pos2 == 0)) {
        temp = list[pos1];
        list[pos1] = list[pos2];
        list[pos2] = temp;
        scanf("%d %d", &pos1, &pos2);
    }

    bool is_ascending = true;

    for(int i = 0; i < list_size - 1; i++) {
        if (list[i] > list[i + 1]) {
            is_ascending = false;
            break;
        }
    }

    if (is_ascending) {
        printf("YES");
    } else {
        printf("NO");
    }

    free(list);
    return 0;
}
