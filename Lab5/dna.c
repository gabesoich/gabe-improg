/* file: dna.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 02/10/2018 */
/* version: 0.0.1 */
/* Description: matches substrings of a given genome string for multiple genomes */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

void *safeMalloc(size_t size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeCalloc(size_t size) {
    void *p = calloc(size, size);
    if (p == NULL) {
        printf("No more memory? calloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

void *safeRealloc(void *p, size_t size) {
    void *temp = realloc(p, size);
    if (temp == NULL) {
        printf("No more memory? realloc(%ld) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return temp;
}

// char array that doubles in size when limit is reached
typedef struct String {
    char *content;
    size_t current_size;
    size_t current_max;
} String;

// functions for String
void initialiseString(String *s, size_t initial_size) {
    s->content = safeCalloc(initial_size * sizeof(char));
    s->current_size = 0;
    s->current_max = initial_size;
}

void addChar(String *s, char to_add) {
    if (s->current_size == s->current_max) {
        s->current_max *= 2;
        s->content = safeRealloc(s->content, s->current_max * sizeof(char));
    }
    s->content[s->current_size++] = to_add;
}

void freeString(String *s) {
    free(s->content);
    s->content = NULL;
    s->current_size = s->current_max = 0;
}
// end functions for String

// returns the offset of the first occurrence of needle in haystack,
// or -1 if not found.
long long stringFind(char *needle, size_t needle_length, char *haystack, size_t haystack_length) {
    long long first_needle_offset = -1;
    bool found = false;

    if (
        needle_length > haystack_length ||
        needle_length == 0 ||
        haystack_length == 0
    ) {
        return first_needle_offset;
    }

    // only 1 possible if same length, 2 if haystack 1 char longer etc
    size_t num_possible_substrings = haystack_length - needle_length + 1;
    
    for (size_t i = 0; i < num_possible_substrings; i++) {
        if (found) {
            break;
        }
        // check every char of needle
        for (size_t j = 0; j < needle_length; j++) {
            if (haystack[i + j] != needle[j]) {
                break;
            }
            if (j + 1 == needle_length) {
                found = true;
                first_needle_offset = i;
            }

        }
    }

    return first_needle_offset;
}

typedef struct Person {
    String name;
    String genome;
    long long first_match_offset;
} Person;

void setNameAndGenome(Person *person) {
    char current_char;
    
    initialiseString(&person->name, 64);
    initialiseString(&person->genome, 64);

    // read the name
    while (1) {
        current_char = getchar();
        if (current_char == ':') {
            break;
        }
        addChar(&person->name, current_char);
    }
    // read the genome
    while (1) {
        current_char = getchar();
        if (current_char == '\n') {
            break;
        }
        addChar(&person->genome, current_char);
    }

}

void freeThePeople(Person *people, int num_people) {
    for (int i = 0; i < num_people; i++) {
        freeString(&people[i].name);
        freeString(&people[i].genome);
    }
    free(people);
}

int main(int argc, char *argv[]) {

    String pattern;
    initialiseString(&pattern, 32);

    int current_char;
    while(1) {
        current_char = getchar();
        if (current_char == '\n') {
            break;
        }
        addChar(&pattern, current_char);
    }

    int num_people;
    scanf("%d\n", &num_people);

    Person *people = safeMalloc(num_people * sizeof(Person));
    for (int i = 0; i < num_people; i++) {
        setNameAndGenome(&people[i]);
        people[i].first_match_offset = stringFind(
            pattern.content,
            pattern.current_size,
            people[i].genome.content,
            people[i].genome.current_size
        );
    }

    freeString(&pattern);

    int num_first_matches = 0;
    for (int i = 0; i < num_people; i++) {
        if (people[i].first_match_offset != -1) {
            printf("%s: shift=%lld\n", people[i].name.content, people[i].first_match_offset);
            num_first_matches++;
        }
    }

    printf("%d %s found.", num_first_matches, num_first_matches != 1 ? "matches" : "match");

    freeThePeople(people, num_people);
    people = NULL;

    return 0;
}
