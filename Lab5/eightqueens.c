/* file: eightqueens.c */
/* author: Gabriel Soicher (email: g.o.soicher@student.rug.nl) */
/* date: 02/10/2018 */
/* version: 0.0.1 */
/* Description: determines whether a given arrangement of queens is valid
    for the eight queens problem */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void *safeMalloc(int size) {
    void *p = malloc(size);
    if (p == NULL) {
        printf("No more memory? malloc(%d) failed.\n", size);
        exit(EXIT_FAILURE);
    }
    return p;
}

typedef struct Queen {
    int row, column;
} Queen;

bool columnOrRowIsDuplicated(Queen *queens, int row, int column, int num_queens) {
    for (int i = 0; i < num_queens; i++) {
        if (queens[i].row == row || queens[i].column == column) {
            return true;
        }
    }
    return false;
}

int **createBoard() {
    int **board = safeMalloc(8 * sizeof(int *));
    for (int row = 0; row < 8; row++) {
        board[row] = safeMalloc(8 * sizeof(int));
    }
    for (int row = 0; row < 8; row++) {
        for (int col = 0; col < 8; col++) {
            board[row][col] = 0;
        }
    }
    return board;
}

void printBoard(int **board) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            printf("%d ", board[i][j]);
        }
        printf("\n");
    }
    printf("\n\n");
}

void clearBoard(int **board) {
    for (int i = 0; i < 8; i++) {
        free(board[i]);
    }
    free(board);
}

bool diagonalsAreClear(int **board, Queen q) {
    // up and right diag
    board[q.row][q.column] = 1;
    for (int row = q.row + 1, col = q.column + 1; row < 8 && col < 8; row++, col++) {
        if (board[row][col] == 1) {
            return false;
        }
    }
    // up and left diag
    for (int row = q.row + 1, col = q.column - 1; row < 8 && col >= 0; row++, col--) {
        if (board[row][col] == 1) {
            return false;
        }
    }
    // down and right diag
    for (int row = q.row - 1, col = q.column + 1; row >= 0 && col < 8; row--, col++) {
        if (board[row][col] == 1) {
            return false;
        }
    }
    // down and left diag
    for (int row = q.row - 1, col = q.column - 1; row >= 0 && col >= 0; row--, col--) {
        if (board[row][col] == 1) {
            return false;
        }
    }
    return true;
}

int main(int argc, char *argv[]) {
    bool invalid_arrangement = false;

    Queen *queens = safeMalloc(8 * sizeof(Queen));

    int row;
    char column; 
    for (int i = 0; i < 8; i++) {
        scanf("%c%d,", &column, &row);
        // we're using zero indexing
        column--;
        row--;
        if (columnOrRowIsDuplicated(queens, row, column, i)) {
            invalid_arrangement = true;
            break;
        }
        queens[i].row = row;
        // a -> 1, b -> 2 etc
        queens[i].column = column - 96;
    }

    if (invalid_arrangement) {
        free(queens);
        printf("INVALID");
        return 0;
    }

    int **board = createBoard();

    for (int i = 0; i < 8; i++) {
        if (!diagonalsAreClear(board, queens[i])) {
            invalid_arrangement = true;
            break;
        }
    }

    if (invalid_arrangement) {
        printf("INVALID");
    } else {
        printf("VALID");
    }

    free(queens);
    free(board);
    return 0;
}
